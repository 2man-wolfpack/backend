$(document).ready(function() {


  var $hamburger = $(".hamburger");
  $hamburger.on("click", function(e) {
    $hamburger.toggleClass("is-active");
    $(".mobile-nav").toggleClass("responsive");
  });

  $(".menu-wrap").on("click", "li", function () {
      $(".hamburger").click();
  });


});


$(document).ready(function() {

  $('input[name=transport]').change(function(){
      if($('#transport-courier').is(':checked'))
      {
        $('#automaadid').hide();
      }
      if($('#transport-pakk').is(':checked'))
      {
        $('#automaadid').show();
      }
  });




  $('#gift-form-submit').on('click', function() {

    var validation = true;
    $('.validate').each(function() {
      if (validate($(this)) === false)
      {
        validation = false;
      }
    });

    if ( ! $('#disclaimer').is(':checked'))
    {
      $('label[for=disclaimer]').css('color', 'red');
      $('#disclaimer').change(function(){
          $('label[for=disclaimer]').css('color', 'black');
      });
      validation = false;
    }

    if (validation === true)
    {
      $('#gift-form').submit();
    }
  });

});


// Accepted rules: required|email|numeric|not:1,2,3
var validate = function(elem) {

  var rules = elem.data('validate').split("|");
  var correct = true;

  rules.forEach(function(rule){
    if (rule === 'required')
    {
      if (elem.val() === '')
      {
        displayError(elem);
        correct = false;
      }
    }
    else if(rule === 'email')
    {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if(!emailReg.test(elem.val())) {
          displayError(elem);
          correct = false;
        }
    }
    else if (rule.substr(0,3) === 'not')
    {
      rule.substr(4).split(',').forEach(function(not){
        if (elem.val() === not)
        {
          displayError(elem);
          correct = false;
        }
      })
    }
    else if(rule === 'numeric')
    {
      if (!$.isNumeric(elem.val()))
      {
          displayError(elem);
          correct = false;
      }
    }
  });

  return correct;
};

var displayError = function(elem) {
  elem.css('border', '1px solid red')
      .change(function() {
        elem.css('border', 'none');
      });
};