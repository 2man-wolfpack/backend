<?php namespace Lightweber\Raamatud\Models;

use Model;

/**
 * Model
 */
class Tellimus extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    public $fillable = [
        'eesnimi',
        'perenimi',
        'uulits',
        'maja',
        'korter',
        'indeks',
        'asula',
        'maakond',
        'email',
        'telefon',
        'mobiil',
        'transport',
        'automaat',
        'new',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'lightweber_raamatud_tellimus';

    public $belongsTo = [
        'pakiautomaat' => [
            Pakiautomaat::class,
            'key' => 'automaat',
            'other_key' => 'kood',
        ],
    ];


    public function getNimiAttribute()
    {
        return trim(($this->eesnimi ?: '') . ' ' . $this->perenimi);
    }
}