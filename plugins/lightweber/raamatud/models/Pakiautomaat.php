<?php namespace Lightweber\Raamatud\Models;

use Model;

/**
 * Model
 */
class Pakiautomaat extends Model
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'kood';

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $fillable = [
        'kood',
        'nimetus',
        'aadress',
        'maakond',
    ];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'lightweber_raamatud_automaat';
}