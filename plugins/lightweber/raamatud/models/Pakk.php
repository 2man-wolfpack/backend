<?php namespace Lightweber\Raamatud\Models;

use Model;

/**
 * Model
 */
class Pakk extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $fillable = [
        'nr',
        'color',
    ];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'lightweber_raamatud_pakk';

    public $hasMany = [
        'raamatud' => Raamat::class,
    ];
}