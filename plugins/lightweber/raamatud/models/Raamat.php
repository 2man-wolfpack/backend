<?php namespace Lightweber\Raamatud\Models;

use Carbon\Carbon;
use Model;

/**
 * Model
 */
class Raamat extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $fillable  =[
        'nimi',
        'slug',
        'autor',
        'pakk_id',
        'date',
        'pilt',
        'sisu',
    ];

    protected $jsonable = ['highlights'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'lightweber_raamatud_raamat';

    public $belongsTo = [
        'pakk' => Pakk::class,
    ];

    public function getDaatumAttribute()
    {
        return Carbon::parse($this->date)->format('d.m.Y');
    }

    public function getDaatumKuugaAttribute()
    {
        setlocale(LC_TIME, 'Estonian');

        return Carbon::parse($this->date)->formatLocalized('%d %B %Y');
    }

    public function getFormatedNamesAttribute()
    {
        $text = $this->autor . ' „' . $this->nimi . '“';

        if($this->highlights)
        {
            foreach($this->highlights as $hl)
            {
                $highlait = array_get($hl, 'text');

                if ($highlait)
                {
                    $text = str_replace($highlait,'<span class="highlighted">' . $highlait . '</span>' ,$text);
                }
            }
        }
        return $text;
    }
}