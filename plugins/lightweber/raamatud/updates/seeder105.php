<?php namespace Lightweber\Raamatud\Updates;

use Carbon\Carbon;
use Lightweber\Raamatud\Models\Raamat;
use Seeder;

class Seeder105 extends Seeder
{

    protected $dates = [
        '5/4/17',
        '5/11/17',
        '5/18/17',
        '5/25/17',
        '6/1/17',
        '6/8/17',
        '6/15/17',
        '6/22/17',
        '6/29/17',
        '7/6/17',
        '7/13/17',
        '7/20/17',
        '7/27/17',
        '8/3/17',
        '8/10/17',
        '8/17/17',
        '8/24/17',
        '8/31/17',
        '9/7/17',
        '9/14/17',
        '9/21/17',
        '9/28/17',
        '10/5/17',
        '10/12/17',
        '10/19/17',
        '10/26/17',
        '11/2/17',
        '11/9/17',
        '11/16/17',
        '11/23/17',
    ];

    protected $authors = [
        'Kulle Raig',
        'Paul Tamm',
        'Margit Kilumets',
        'Endel Nirk',
        'Mari Tarand',
        'Madli Puhvel',
        'Eino Baskin ja Toomas Kall',
        'Küllo Arjakas',
        'Erik Schmidt',
        'Ene Hion',
        'Rutt Hinrikus',
        'Rein Taagepera',
        'Mati Laos',
        'Uku Masing',
        'Hille Karm',
        'Heinz Valk ja Madis Jürgen',
        'Kirsti Vainküla',
        'Juhan Liiv',
        'Tiina Laanem',
        'Alo Lõhmus',
        'Tiit Kändler',
        'Andres Laasik',
        'Dagmar Reinolt ja Silvi Vrait',
        'Voldemar Panso',
        'Erast Parmasto',
        'Jüri Arrak',
        'Gunnar Press ja Voldemar Lindström',
        'Toivo Tootsen',
        'Paavo Kangur',
        'Kulle Raig',
    ];

    protected $names = [
        'Vikerkaare värvid. Lennart Meri elu sõprade pilgu läbi',
        'Meenutusi Paul Keresest',
        'Ita Ever. Elu suuruses',
        'Kaanekukk. Lugu Ants Laikmaa elust ja ettevõtmistest ',
        'Ajapildi sees. Lapsepõlv Juhaniga',
        'Lydia Koidula. Elu ja aeg',
        'Otse läbi lillede. Vestlus Toomas Kalliga',
        'Konstantin Konik. Unustatud suurmees',
        'Minu onu Bernhard Schmidt',
        'Valdo Pant – aastaid hiljem',
        'Marie Underi päevikud 1922-1957',
        'Mare ja mina. Mälestusi 1960–1970',
        'Jüri Kraft. Härra seltsimees minister',
        'Kirjad Liile',
        'Helgi Sallo. Helgib ja heliseb',
        'Lugu poisist, keda kallistas ilus pastoriproua',
        'Ervin Abel. Siin ma olen',
        'Mu kallis Liisi',
        'Kihnu Virve. Nagu linnukene oksal',
        'Ööbikut ei tohi reeta. Rein Marani elu filmides',
        'Endel Lippmaa. Mees parima ninaga',
        'Kaljo Kiisk. Ikka hea pärast',
        'Silvi Vrait. Saatus',
        'Portreed minus ja minu ümber',
        'Ühe seenevana elupäevad',
        'Võsast mäeni',
        'Uba. Toomas Uba',
        'Vana Hõbe. Hardi Tiidus ja inimesed tema ümber',
        'Urmas Alender. Lõputa lookleval teel',
        'Saaremaa valss. Georg Otsa elu',
    ];

    protected $pics = [
        '01.jpg',
        '02.jpg',
        '03.jpg',
        '04.jpg',
        '05.jpg',
        '06.jpg',
        '07.jpg',
        '08.jpg',
        '09.jpg',
        '10.jpg',
        '11.jpg',
        '12.jpg',
        '13.jpg',
        '14.jpg',
        '15.jpg',
        '16.jpg',
        '17.jpg',
        '18.jpg',
        '19.jpg',
        '20.jpg',
        '21.jpg',
        '22.jpg',
        '23.jpg',
        '24.jpg',
        '25.jpg',
        '26.jpg',
        '27.jpg',
        '28.jpg',
        '29.jpg',
        '30.jpg',
    ];

    public function run()
    {
        Raamat::create([
            'nimi'      => $this->names[0],
            'slug'      => str_slug($this->names[0]),
            'autor'     => $this->authors[0],
            'pakk_id'   => 1,
            'date'      =>  Carbon::parse($this->dates[0]),
            'pilt'      => '/storage/app/media/raamatud/' . $this->pics[0],
            'sisu'      => $this->sisu[0],
        ]);

        for ($i = 3; $i < count($this->authors) + 2; $i++)
        {
            $pakk = intdiv($i + 8, 5);

            $pilt = '/storage/app/media/raamatud/' . $this->pics[$i - 2];

            Raamat::create([
                'nimi'      => $this->names[$i - 2],
                'slug'      => str_slug($this->names[$i - 2]),
                'autor'     => $this->authors[$i - 2],
                'pakk_id'   => $pakk,
                'date'      =>  Carbon::parse($this->dates[$i - 2]),
                'pilt'      => $pilt,
                'sisu'      => $this->sisu[$i-2],
            ]);
        }
    }

    protected $sisu = [

            "
         <p>„Vikerkaare värvid” jutustab Eesti presidendi Lennart Meri (1929 – 2006) erinevatest, kohati dramaatilistest eluperioodidest tema sõprade, töökaaslaste ja lähemate abiliste pilgu läbi. Autor on oma raamatus lühidalt kokku võtnud, kuidas Merist sai Eesti iseseisvusmõtte kandja, uue Eesti oluline poliitiline mõjutaja ning lõpuks president. Lennart Meri tekstidest kostab eurooplase hääl: ta on elav näide meie maailmajao kultuuri nurgakivist, euroopalikust mitmekesisusest, mille tähtsust rõhutades kasutas ta sümbolina tihti vikerkaare värve. Sellest sümbolist on ajendatud ka käesoleva raamatu pealkiri.</p>
         
         <p>Lennart Meri nimega on otseselt seotud Eesti Vabariigi iseseisvuse taastamise pöördeline aeg, kuid ta oli ka filmimees ning reisikirjade ja ajalooraamatute autor, soome-ugri pärandi talletaja. Kunstilise loojana oli tema missioon sarnane hilisema poliitikukarjääri kreedoga: jutustada Eesti, Euroopa ühe põliseima rahva lugu.</p>
         ",
            "<p>Postimehe ajakirjanik ja malenurga toimetaja Paul Tamm jutustab oma koostööst Paul Keresega (1916 – 1975). Tema jutustus on erakordne. Paul Keresega tutvus ta juba 1931. aastal, kui tulevane suurmeister oli alles 15-aastane. Tamm võttis tol suvel ette jalgrattaretke Pärnusse, et oma silmaga näha noormeest, kes saadab Postimehele maleülesandeid. Sellest sai alguse sõprus ja viljakas koostöö. Tamm ja Keres töötasid koos nii Postimehe malenurkade, kui ka 1936 – 1940 ilmunud ajakirja Eesti Male kallal. Tamm saatis Kerest 1937. aastal Kemeri turniiril ja oli samal aastal korrespondendina koos Eesti meeskonnaga Stockholmi maleolümpial. Kerese ja Tamme vahel oli tihe kirjavahetus, mida Tamm oma käsikirjas ka ohtralt tsiteerib.</p>",
            "
         <p>Autor Margit Kilumets pole võtnud teatriloolase kohustust üksipulgi lahata armastatud näitlejanna teatri-, filmi- ja telerolle. Pigem on see raamat Ita Everist kui kolleegist, naisest, emast, vanaemast koos pildimaterjaliga. Ita Ever ise tunnistab, et pole kunagi varem olnud nii avameelne. Autori meelest on aga Ita-nimelise piltmõistatuse kokkupanekust kujunenud tema senise ajakirjanikukarjääri suurim väljakutse.</p>
         
         <p>See töö on olnud... priima!, kui kasutada näitlejanna enda lemmikväljendit.</p>
         
         <p>Noorpõlves oli ta kuulus oma punaste juuste ja roheliste silmade poolest. Need on sassi ajanud nii mõnegi mehe pea. Ta armastab lihtsat elu ja lihtsaid asju.</p>
         
         <p>„Uhkeldamine ei sobi sellele, kes on eluaeg puuvirnaga vastamisi elanud,“ leiab ta.</p>
         
         <p>Ta on pärit põlvkonnast, keda iseloomustavad sellised märksõnad nagu eetika, lavameisterlikkus, kohutav töö, eneseohverdamine. Kogu muu elu jääb seisma, kui mängu tuleb teater.</p>
         
         <p>Ta on näitlejanna, keda eesti rahvas tingimusteta armastab.</p>
         
         <p>Ta on Ita Ever.</p>
         ",
            "
         <p>Ants Laikmaa (1866 – 1942) oli üks Eesti kunstielule alusepanijaid 20. sajandi alguses, kelle roll nii looja, õppejõu kui kunstielu organiseerijana on hindamatu. Kunstnikuna annavad Laikmaa loomingus tooni pastellmaalid: pastellidega ta lõi nii maastikke kui portreesid. Laikmaaga seostatakse ka impressionismi tulekut eesti kunsti. Ta õppis lühiajaliselt Peterburis ja veidi pikemalt Düsseldorfis, kuid alates 1901. aastast elas taas Eestis. Kaks aastat hiljem asutas ta ateljeekooli, millest kujunes oluline õppeasutus ja 20 aasta jooksul õppis seal ligi 800 inimest.</p>
         
         <p>Nii Laikmaa kunst kui ka kunstialane tegevus lähtus rahvuslikest ideaalidest. Oma kunstis portreteeris ta korduvalt märgilisi isikuid (nt Marie Under, Miina Härma, Fr. R. Kreutzwald, eestlastest talupojad jne) või kujutas kodumaa loodust. Eraldi peatüki moodustavad Laikmaa 1910ndate alguses valminud tööd Itaalias, Capril, Tuneesias jm. Alates 1920ndatest osales Laikmaa näitustel harva ning tõmbus 1930ndatel aktiivsest kunstielust tagasi, sulgedes ka oma ateljeekooli.</p>
         
         <p>„Kaanekuke“ kirja pannud Endel Nirk on eesti kirjandusteadlane, -kriitik ja kirjanik, Friedrich Reinhold Kreutzwaldi ja Karl Ristikivi uurija ning ka üks 40 kirjale allakirjutanutest 1980. aastal.</p>
         ",
            "
         <p>Raamatu peategelaseks on eesti poeet, näitleja ja lavastaja Juhan Viiding. Aga noorena, nagu teda on näinud ja mäletab õde Mari Tarand. Autor on teadlikult vältinud materjali kogumist kaugemalt, Juhani sõprade ja teekaaslaste suust, mistõttu on tegemist väga isikliku elulooga, pigem mälestusteraamatuga.</p>
         
         <p>Kirjutatud on perest ja kodust, pinnasest, kus tulevane luuletaja kasvas. Selle andeka ja mitmekülgse pere liikmete ja seega raamatu tegelaste hulka kuulub ka kirjandus – luule. See on ühendanud, tuge andnud nii kirjutajatele kui nende lugejatele.</p>
         ",
            "
         <p>Lydia Koidula (1843 – 1886) nimi oli juba ta eluajal tuntud, kuid tõeliselt suur tunnustus sai talle osaks alles pärast surma. Tänapäeval peetakse teda Eesti rahvuskangelannaks, kelle pilt ehib postmarke ja trükiti pärast iseseisvuse taastamist Eesti Vabariigi 100-kroonisele rahatähele.</p>
         
         <p>Kui Madli Puhveli raamat „Symbol of Dawn“ 1995. aastal ingliskeelsena ilmus, kirjutas Märt Väljataga: „Ometigi on ingliskeelsed lugejad praegu privilegeeritud olukorras. Sest nii head Koidula elulugu kui […] California ülikooli meditsiiniprofessori Madli Puhveli raamat „Symbol of Dawn“ eesti keeles ei ole. […] Iga peatükk Koidula eluloost annab ühtlasi panoraamse vaate sellest, mis toimus Eesti poliitika-, seltskonna- ja vaimuelus. Teisalt on eluloolane olnud hoolikas ajastu olmelise poole rekonstrueerimisel.”</p>
         ",
            "
         <p>Eesti teatrihiiglane, armastatud näitleja ja lavastaja Eino Baskin (1929 – 2015) vestleb siin raamatus pikemalt oma kauaaegse sõbra ja kolleegi, eesti näitekirjaniku, humoristi ja ajakirjaniku Toomas Kalliga.</p>
         
         <p>„Lähed lilledega läbi garderoobi, võtad grimmi maha - see on väga hea tunne. Aga lähed tänavale, ja ongi läbi kogu see eufooria! Sest seal võib olla juba bandiit, nuga taskus!</p>
         
         <p>Näiteks eile istusin volikogus ja veendusin järjekordselt, et kui oleksid head kirjanikud ja teravmeelsed näitlejad näinud seda jama, mis seal toimub, siis oleks sellest saanud sihukese estraadietenduse, et siga ka ei söö.</p>
         
         <p>Ma arvan, et mina annan Kroonikale rohkem lugejaid juurde kui Kroonika minule publikut.</p>
         
         <p>Platooniline vahekord, naine kui sõber, ei ole mind elus kunagi huvitanud.</p>
         
         <p>Ma tegin väga valesti, et ma teatri ära andsin. See oli minu elu kõige suurem viga.“</p>
         ",
            "
         <p>Konstantin Konikut (1873 – 1936) teavad tänapäeva Eesti Vabariigis vähesed – vahest mõni arst ning ajaloohuviline. Praegused gümnaasiumi ajalooõpikud mainivad tema nime kõigest korra. Konik oli kuulus kirurg ning teenekas Tartu Ülikooli arstiteaduskonna dekaan. Ta kuulus Konstantin Pätsi ja Jüri Vilmsi kõrval kolmanda liikmena Eestimaa Päästmise Komiteesse. Nemad kuulutasid 1918. aasta veebruaris välja Eesti Vabariigi ja panid nii aluse peagi saja-aastaseks saavale omariiklusele.</p>
         
         <p>Konstantin Konikul oli elus palju õnne: ta pääses tervelt Vene-Jaapani sõjast ega nakatunud pikkadel haiglatööaastatel mõnda raskesse haigusse. Tal oli õnn jääda ellu Eesti Vabadussõja lõpu aegses tüüfuseepideemias. Tal oli õnn saavutada mitmes valdkonnas silmapaistvaid tulemusi ning lahkuda siit ilmast enne Eesti Vabariigi hukku teadmisega, et tema alustatud ja tehtud tööd jätkatakse vääriliselt.</p>
         ",
            "
         <p>Iga astronoom teab, mis on Schmidti kaamera -- seda tüüpi teleskoopidega tehtud fotodest on koostatud kõige täiuslikumad taevaatlased. Paljudele jäi aga kauaks teadmata, et selle kaamera leiutanud optik Bernhard Schmidt (1879 – 1935) oli pärit Eestist, Naissaarelt.</p>
         
         <p>Eesti optiku, astronoomi ja leiutaja Bernhard Schmidti heitlikku ja vaevarikast teed maailmakuulsaks leiduriks saamisel kirjeldab haaravalt tema Mallorcal elanud vennapoeg Erik Schmidt, eestirootsi kunstnik ja kirjanik, kes rikastab jutustust isiklike muljete ja mälestustega oma onust ning 20. sajandi alguse Eestist ja Saksamaast.</p>
         ",
            "
         <p>Eesti ajakirjanik, raadio- ja telesaatejuht Valdo Pant (1928 – 1976) oli omaette suurkuju, epohhi loov isiksus Eesti raadio- ja teleajakirjanduse ajaloos. Paneb imestama, milliste ideede ja lahendustega tuli ta välja ajal, kui rahvusvaheline infovoog oli Eesti jaoks piiratud ja kui tänapäeva tehnilistest võimalustest ei osatud veel unistadagi. Seda enam nõudis esiletõus ja publikumenu temalt loomingulisust, leidlikkust ja kõneosavust. Suisa uskumatuna tundub Pandi töövõime ja nimekiri töödest, mida ta raadio- ja tele-, aga ka filmi- ja kirjandusmaastikul ära tegi.</p>
         
         <p>Võiks arvata, et peale töö tema ellu midagi enamat ei mahtunudki. Aga ometi – tuleb välja, et ta oli ka värvikas seltskonnategelane ja hoolitsev pereisa. Mis aga kõige üllatavam, tema elu saatsid pidevalt isiksuse lõhestatus, näilikkus, laveerimine võimu ja tõe vahel, jälgede segamine ja oma mineviku ümberkirjutamine. Sellel kõigel oli siiski hind – tema piinatud eneseteadvuse, meeletu töökoormuse ja nooruspõlvest pärit tervisehäda koorem paisus lõpuks nii suureks, et murdis ka sellise sitke mehe. Valdo Pant ohverdas oma tööle ande, armastuse, tervise, elu ja võib-olla ka tunnustuse. Ometigi võib tema kohta julgelt kasutada sõna „valgustaja“.</p>
         ",
            "
         <p>Kelle oma on Marie Underi päevik? Kes pidas Underi päevikut? Isepäine, nagu koostaja Rutt Hinrikus oma saatesõnas märgib, on see päevik tõesti.</p>
         
         <p>1914. aastal kingib Artur Adson oma armastatule sünnipäevaks hõbedaste leheservadega ning initsiaalidega MU ja kuupäevaga 15. III 1914 varustatud hoolikalt köidetud kaustiku. Möödub kaheksa aastat ning alles siis hakkavad selle valged lehed täituma – algul Underi enda, hiljem aga üha enam Adsoni käekirjas sissekannetega. Ühele päevikuköitele lisandub ajas rööbiti teine, lahtistel lehtedel kolmaski.</p>
         
         <p>Nende raamatukaante vahel ajaliselt järjestatuna annavad need päevikumärkmed fragmentaarse sissevaate nii luuletajapaari argimuredesse kui ka harvadesse pühapäevadesse, olles tänu Adsoni jäädvustamistarbele väärtuslikuks lisanduseks meie suure ja aegumatu luuletaja, kuid eelkõige ühe sureliku inimese, tundmaõppimise teel.</p>
         ",
            "
         <p>„Mare ja mina. Mälestusi 1960–1970“ on Eesti politoloogi ja poliitiku Rein Taagepera mälestusteseeria teine raamat, milles Mare ja Rein Taagepera loovad perekonna, õpingud asenduvad mõlemal tööga. Igapäevast elu saadab soov mitte jääda vaikivaks kõrvaltvaatajaks Põhja-Ameerika väliseestlaste maailmas.</p>
         
         <p>Eesti pagulaskonna poolt vaadatuna oli see aastakümme autori arvates murranguline. Kodumaa ja asukohamaa kuvandi kõrvale ilmus uue ja vana kodumaa kuvand. Raamatus leiab muuhulgas käsitlemist teema, milline oli tollal pagulaste suhtumine okupeeritud Eesti Vabariiki. Osa pagulastest arvas, et peab säilima mineviku-Eesti ja kuna sellist Eestit enam ei ole, ei tohi pagulane Eestit NSV-d külastada.</p>
         
         <p>Teine pool, kelle hulka kuulub ka selle raamatu autor, pidas õigemaks tollases situatsioonis proovida midagi Eesti heaks ära teha. Sellest soovist sündiski satelliitsusmemorandum (nimetatud ka Ungari teeks), katse Eestit Nõukogude Liidust välja nihutada vähemalt Ungari-laadsesse olukorda. Kahjuks ei täitnud algatus oma eesmärki ja oli paljuski valesti mõistetud ning viis lausa Rein Taagepera väljaheitmiseni Eesti Üliõpilaste Seltsist.</p>
         ",
            "
         <p>Eesti majanduselu üks mõjukamaid ja tegusamaid suguvõsasid on Kraftide pere. Selles raamatus räägib majandustegelane, pikaajaline tööstusjuht ja endine Eesti NSV riigitegelane Jüri Kraft Eesti esimesel iseseisvusperioodil alanud lapsepõlvest Kasepää vallas, koolipoisiaastatest Mustvees, õpingutest Tartu Ülikooli majandusteaduskonnas, tööst õmblusvabriku Sangar direktorina, aseministri ja ministrina ning tegevusest ettevõtluses ja ühiskonnaelus.</p>
         
         <p>See on lugu mehest, kes kõige raskematel aegadel suutis enese jaoks ilmsiks muuta ameerikaliku unistuse kasvades saapapuhastajast miljonäriks. Tänaseks suurest poliitikast taandunud Jüri Krafti tegemisi on jäänud märkima pea 14 aastat kestnud kergetööstusministri karjäär, ilmselt miljardite eest Eestimaale ehitatud tootmis- ja olmeobjekte, MRP-pakti avalikustamine Moskvas ning tänane tegevus Eesti ajaloo suurkujude mälestuse mitmekülgsel jäädvustamisel.</p>
         ",
            "
         <p>Eesti teoloog, orientalist ja filosoof ning vast kuulsaim polüglott Uku Masing (1909 – 1985) tutvus nooruspõlves Alide Klemetsaga silmakliinikus – mõlemad noored vajasid tugevaid prille. Mõlemad olid ka põhjaeestlased, üks Harjumaalt, teine Virumaalt. Masingu kiindumus oli siiralt usalduslik, kirjade toon pihtimuslik, ent juba neiski on takkajärgi tuntav Masingu poeetiline liialduskalduvus.</p>
         
         <p>Kirjades Alide (Lii) Klemetsale kirjeldatakse ka poja vahekorda emaga. Hugo kaebab, et ema on tema vastu ülekohtuselt karm, aga miks? Seepärast, et poeg Hugo on liiga erinev teistest noortest, ei ole „nagu teised“. Võiks öelda, et poja omapärase andekuse mõistmiseks puudus tema emal vajalik haare, ema usaldas ainult valju korda ja rangust, aga sellest ei piisanud pojale. Masingu vaimse arengu kiirus oli tohutu. Stipendiaadiaastad Saksamaal küpsetasid teda erilise kiirusega. Ta vaim kaugenes ka armsa Lii vaimust. Viimases kirjas teatab Masing kurva aususega: oled vist isegi märganud, et kogu viimase aasta jooksul ei ole sa enam mu hinges. Ilus ja kurb nagu Shakespeare'i tragöödia! Aga kõik see polnudki väljamõeldis, vaid oli päriselt.</p>
         ",
            "
         <p>Hille Karmi raamatust saab lugeja teada nii mõndagi armastatud ja auhinnatud eesti laulja ja näitleja Helgi Sallo elust ning mõtetest. Helgi Sallo ei ole saanud muusikalist eriharidust ja ometi teatakse teda peamiselt imelise häälega lauljana.</p>
         
         <p>Siin raamatus räägib Helgi asjadest, mis on meile kõigile olulised. Need on nimelt armastus, lapsed, kodu, kaaslased, elu ja teater. Lugejal on võimalus elada kaasa loole, kuidas sai lummava lauluhäälega noorukesest naisest see Helgi Sallo, keda me kõik tunneme ja teame.</p>
         ",
            "
         <p>See raamat keskendub eesti kunstniku ja poliitiku, Rahvarinde ühe asutajaliikme Heinz Valgu lapsepõlvele ja poisieale. „Ühtlase ilusa käekirjaga, imepeene tindipliiatsiga on paksu nahkkaantega kladesse kirjutatud üks lugu. Koletust pommiraginast sõjaaegses Gattšinas; kodusest praeliha-lõhnast Virumaa rehetoas; kibedast puudusest sõjajärgses Tallinnas. See on läbielatud ajalugu, mille on oma lastelaste jaoks paeluvalt taaselustanud üks siitkandi säravamaid vanamehi – Heinz Valk.“</p>
         
         <p><em>Madis Jürgen</em></p>
         
         <p>„Minu kallitele lastelastele Sofiale, Konradile, Sanderile, Marile, Andreasele, Jakobile, Annile, Rihardile ja Tobiasele selle tõestuseks, et ma polegi alati olnud vanaisa, vaid ammusel ajal ka nendetaoline jõmpsikas. Näputäis meenutusi, mis kinnitavad, et inimese iseloom, tema väärtusmõisted ning teod saavad alguse lapsepõlves kogetust.“</p>
         
         <p><em>Heinz Valk</em></p>
         ",
            "
         <p>Legendaarne eesti teatri- ja filminäitleja Ervin Abel (1929 – 1984) sai tuntuks ja armastatuks osatäitmistega kirjanik Oskar Lutsu teostel põhinevates lavastustes ja filmides. Eesti filmiajalukku on ta jäänud osatäitmistega komöödiafilmides „Viini postmark“, „Mehed ei nuta“, „Noor pensionär“ ja „Siin me oleme!“.</p>
         
         <p>Abel ütles kaheksa kuud enne surma: „Minu saatus on olnud õnnelik. Mul pole olnud tegevusetust.“ Selleks ajaks oli ta teinud viimased 17 aastat peaaegu puhkamata estraadikunsti. Kui ta polnud laval, siis esines ta raadios või televisioonis. Või viibis tähtsal koosolekul. Praegu öeldakse temasuguste kohta töönarkomaan. Ta soovis olla publikule kergesti mõistetav. Sama eesmärk on Kirsti Vainküla raamatul, kus Ervin Abelit meenutab poolsada inimest.</p>
         ",
            "
         <p>Selle raamatu kaante vahele on koondatud eesti poeedi Juhan Liivi (1864 – 1913) kirjad tema armastatu Liisa Marie Goldingule, Pandivere mõisa kutsari tütrele. Juhan Liivi austajad saavad neist rohkesti andmeid Juhan Liivi kui inimese kohta. Ta on liigutavalt avameelne ja aus, kui meeles pidada, et kirjutas inimesele, kelle olemasolu talle erakordselt palju tähendas ja keda ta kartis kaotada.</p>
         
         <p>Kirjadest võib lugeda armastatud poeedi hingeseisundite, enesetunde, püüdluste ja armutundmuste ning samuti ajakirjandusliku ja kirjandusliku tegevuse kohta. Pidevalt on autor mures ja kimpus oma tervisega. Korduvalt rõhutab Liiv vaesust, mis takistab teda ka Liisaga abiellumast. Rahapuudusel jääb teostamata unistus koos Liisaga oma ajalehte välja andma hakata; hiljem räägib luuletaja raamatukaupluse asutamisest. Ka see mõte jääb ellu viimata.</p>
         ",
            "
         <p>Tiina Laanemi raamat jutustab loo rahvalaulik Virve Kösterist, kes avalikkusele tuntud eelkõige Kihnu Virvena. Biograafia käsitleb nii Virve noorusaastaid Manijal kui ka hilisemat elu Kihnus, samuti tema kujunemist armastatud rahvalaulikuks ja koostööd teiste eesti muusikutega.</p>
         
         <p>Siiruviiruline Virve:</p>
         
         <p>Kui paadipõgenikud suundusid sõja ajal läände, plaanis ka Virve minna, kuid otsustas viimasel hetkel ümber.</p>
         
         <p>Kuuldes kuskilt Zeusi nime, läheb Virve minema. See on tema jaoks nii vastik sõna. Aga üldsegi mitte seetõttu, et kord sai ta välgult tabamuse. Zeusi vihkab ta hoopis teisel põhjusel.</p>
         
         <p>Legendaarne laul „Mere pidu“ sündis Virvel pärast seda, kui uppumissurm oli käega katsuda.</p>
         
         <p>Kuldpulmadeni jõudnud Virve abielust leiab ühtviisi nii päikest kui ka tõelist äikest.</p>
         ",
            "
         <p>Kinomehed imestavad ja lõõbivad selle üle, miks Rein Maran oma soliidse ea kohta nii erakordselt nooruslik välja näeb. Seletus on, et siis, kui ta jälgib kusagil roostikus linde, passib puu otsas hunti, ilvest või kotkast, aeg tema jaoks seisab. Ja loodusfilme on ta teinud juba üle 40 aasta. Rohkem kui poolsada filmi – ühe mehe elutööks on seda arvatavasti küll ja küll.</p>
         
         <p>Rein Marani paljud filmid ei ole klassikalised loodusfilmid, ammugi mitte võrreldavad nende loodus(filmi)sarjadega, mida kannab vaatajani tänane televisioon. Need filmid on omaette nähtus.</p>
         
         <p>Tema filmid on ühelt poolt nähtuna kirju ja kogukas kimp Eestimaa looduse lugusid, nüüd vaat et juba lahutamatu osa eestlase loodusekujutlusest. Ja teisalt annavad need filmid kokku mingi peegelpildi selle maa rahva mentaliteedi süvahoovustest.</p>
         
         <p><em>Jaak Lõhmus</em></p>
         ",
            "
         <p>„Inimestel on arvamus, et lõplikke otsuseid inimesed ei tee. Mina teen. Olen alati teinud.“</p>
         
         <p>Endel Lippmaa (1930 – 2015) alluvuses noore füüsikuna alustanud ja hiljuti Euroopa teaduse populariseerimise auhinnaga pärjatud Tiit Kändler on oma raamatusse mahutanud palju rohkem, kui ühest elulooraamatust oskaks oodata. Kuid eks mahub ka Endel Lippmaasse palju rohkem, kui ühest inimesest oskaks oodata. Lisaks mälestustele ja arhiivifotodele leiab raamatust liigutavaid dokumente Tartu teadlase andeka poja lapsepõlvest, haaravaid sissepõikeid teaduse ajalukku, vaimukaid kirjeldusi oludest, milles Lippmaa ehitas üles oma instituudi ja koolkonna ning rohkesti Šefile iseloomulikke teravmeelsusi ja tervistavat demagoogiat.</p>
         ",
            "
         <p>Selle raamatu kirjutamine algas 2007. aasta hilissuvel. Andres Laasik ja Kaljo Kiisk kohtusid, arutasid raamatu kondikava, otsisid üheskoos vastuseid mõnele selleks ajaks kerkinud küsimusele. Kuid öelda, et raamat on kirjutatud autori ja Kaljo Kiisa koostöös, oleks vale, sest 20. septembril 2007 läks filmilavastaja ja näitleja manalateele.</p>
         
         <p>Kaljo Kiisa jälg Eesti filmikunstis on tohutu, lavastajana jõudis ta teha rohkem mängufilme kui keegi teine. Paljud ta filmid on saanud osaks meie rahvusliku filmograafia kullafondist. Kaljo Kiisa panus teatrisse on oluline ja tipnes näitleja aastapreemiaga. Kiisk jõudis teha ka märkimisväärse poliitikukarjääri, jõudes tähtsatesse valitavatesse ametitesse nii Nõukogude Eestis kui ka taastatud Eesti Vabariigis.</p>
         
         <p>Kaljo Kiisa mängitud rollide, lavastatud filmide ja avalikkuses väljaöeldud sõnavõttude kaudu on tekkinud temast ettekujutus kui muhedast ja heatahtlikust inimesest, kelles on südamlikkust ja hingesoojust. See mulje ei ole petlik, kuid ei ole ka ammendav. Kaljo Kiisa elus oli palju võitlemist oma loomingu eest, enese eest seismist ja ka ebaõnnestumisi, millest teatakse vähem. See raamat püüab rääkida kõigest. Nii nagu Kaljo Kiisk pidas õigeks siis, kui töö selle raamatu kallal oli alles alustamise järgus.</p>
         ",
            "
         <p>Oma elulooraamatus „Saatus“ räägib laulja ja pedagoog Silvi Vrait (1951 – 2013) avameelselt ja sügavuti oma sihtidest ja saavutustest, unistustest ja pettumustest, hoiakutest ja eelarvamustest, kõrghetkedest ja katsumustest. Seda nii läbi muusiku-, ema- kui armastava naise silmade.</p>
         
         <p>Raamatu kirjutamise ajal elu tippvormis olnud laululegend oskab ka oma varasemaid tundeid haaravalt hinnata ja sügavuti analüüsida. Hästi liigendatud raamatut iseloomustab ausus iseenda vastu ja positiivne eluhoiak.</p>
         ",
            "
         <p>Need legendaarse eesti lavastaja, näitleja, teatripedagoogi ja teatriteadlase Voldemar Panso 1960-1970ndatel kirjutatud tekstid on hõrk kultuurilooline maiuspala ka kümneid aastaid hiljem.</p>
         
         <p>„Ma olen saanud selleks, kelleks ma pole unistanud saada. Kelleks ma unistasin saada, selleks ei saanud. Tahtsin saada meremeheks, hiljem klouniks ja veel hiljem kirikuõpetajaks. Ma pole olnud kunagi religioosne inimene; nähtavasti veetles mind õpetajakutseski usuteadus ja sõna elavaks tegemine. Kui vastasin ankeedis, et ihalen klouniks saada, pahandati minu peale gümnaasiumis, et mis mõttes ma end siis harin. Lurichi isa öelnud, kui Georg jäi kindlaks saada jõumeheks: „Oleksin ma sinu kavatsusi varem teadnud, siis poleks ma sind üldse kooli pannud. Ma oleksin selle rahaga, mis sinu koolitamise peale ära raiskasin, mitu head hobust võinud osta.““</p>
         ",
            "
         <p>End Seenevana nime all ajakirjanduses rahvale tuttavaks teinud Erast Parmasto on oma mälestusteraamatus leidlikult ühendanud rahvamehe ja tippteadlase. Ta on jäänud truuks oma loomuomasele lühiloolisele jutustamismudelile, ent on oskuslikult liitnud ajaloolisele printsiibile nn temaatilise printsiibi ning suutnud iga peatüki ajalist mõõtkava laiendada üle ootuste avaraks. Kõneldes kasvõi koolipoiste teadusharrastustest kusagil Tallinna-Nõmme looduses, haarab ta jutustusse sujuvalt ka kaugeid eesseisvaid tulevasi aegu, kus neist koolipoistest on saanud teadlased, poliitikategelased või kestahes.</p>
         
         <p>Jutustaja sünnipärane arukus, millele lisanduvad hindamatult väärtuslikud rahvusvahelised kogemused, võimaldavad liialduseta väita, et Erast Parmasto – Seenevana – raamat iseenda ja oma põlvkonna püüdlustest on iga lugeja meelt ja mõistust liigutav, nooremat lugejat ehk koguni eeskujuandvalt ülendav lugemisvara.</p>
         ",
            "
         <p>Eesti kunsti suurkuju ja elav legend Jüri Arrak kuulub nende kunstnike hulka, kelle nimi ei vaja tutvustamist ka väljaspool kunstiringkondi. „Võsast mäeni“ toob meieni Jüri Arraku kirjatööd, mis artiklitena varem siin-seal ajakirjanduses avaldatud ja kokku panduna toovad selgesti välja autori kreedo kunstniku ja inimesena.</p>
         
         <p>Teemade ring ei ole lai ja selle võiks kokku võtta lihtsalt – kunsti ja kunstniku roll ning nende väärtustamine ühiskonnas. Autorit sunnib tihtipeale haarama sulge südamevalu kunsti sisutühjuse ja mõttetuse pärast tänapäeva maailmas, kus kunsti sildi all saab teha peaaegu kõike. Niiviisi kaobki kunsti põhiline mõte – olla inimkonnale vahendiks Jumala tunnetamise teel.</p>
         
         <p>Kogumiku pealkiri sümboliseerib olulist etappi kunstniku kui ränduri elu- ja loometeel. Religiooni ja eetika mäeni jõudmine peaks autori sõnul olema kunstniku kõige tagasihoidlikum eesmärk.</p>
         ",
            "
         <p>Peaaegu võimatu on leida eestlast, kes ei teaks, kes oli Toomas Uba. Seda teavad kõik, et Uba oli spordiajakirjanik. Ent teatakse rohkemgi: ta oli spordiharitud, töökas, faktitäpne, emotsionaalne, isikupärane.</p>
         
         <p>Üldjuhul teatakse sedagi, et Uba võitles ETV ekraanile välja suured võistlused ja emakeelsed kommentaarid, pruukides oma suurepärast suhtlemisoskust. Vajadusel läks ta visalt ja jõuliselt kas või läbi halli kivi.</p>
         
         <p>Legendaarset Toomas Uba meenutavad pereliikmed, kolleegid Eesti Televisioonist, sportlased ja tuntud eestlased.</p>
         ",
            "
         <p>Kes oli see mees, keda kutsuti omal ajal elavaks entsüklopeediaks – kellel olid silmapaistvad teadmised, aga ka muusikuvõimed, suurepärane keelevaist ja särav jutustamisoskus? Mees, kelle artistlikku natuuri võinuks kadestada ka näitlejad? Kelle seltskonda nauditi ja keda ümbritses alatasa austajateparv? Kui ta oleks saanud valida, kus ja millal elada, eelistanuks ta olla Ateena kodanik ja Sokratese õpilane, kellel on majapidamise tarvis ka paar orja.</p>
         
         <p>Omal ajal ülimalt tuntud ja mõneti vastuolulise Hardi Tiiduse sünnist möödub 2017. aasta sügisel 99 aastat. Hardit ennast pole enam meie seas. Kuid nii nagu ütles kord suur roomlane, ajaloolane Titus Livius – parem hilja, kui mitte kunagi –, nii sai Toivo Tootseni abiga kaante vahele Hardi Tiiduse lugu. Võib öelda, et see lugu valmis paljuski kangelase enda abiga. Raamatusse talletatu on suurel määral Tiiduse suust kuuldud või sulest sündinud. Meenutusi jagavad ka inimesed tema ümbert – tütar, sõbrad ja kolleegid.</p>
         ",
            "
         <p>Urmas Alender (1953 – 1994) oli veerand sajandit Eesti rokimaastiku keskne kuju ja seegi kõlab tema kohta öelduna tagasihoidlikult. Kodumaises rokkmuusika ühe suurima legendi eraelu oli kirev: seks, alkohol ja <em>rock'n'roll</em> kõlavad klišeena, kuid need käisid käsikäes. Naisi tiirles ja vaheldus tema ümber palju, kandvamaks jäid kaks ligi kümne aasta pikkust abielu. Urmas abiellus Liiviga 1976 ja Heljega 1985, aga sinna vahele mahtus hulk fänne, muusasid, austajaid, õnneotsijaid, oma egotõstjaid ja armastusepüüdjaid. Tema esimene armastus oli aga kooliõde Helle, kes kummitas teda aastaid ja kelle näojooned korduvad Urmase hilisemate kallimate ilmetes. Urmas otsis aastaid oma „hellekat“.</p>
         
         <p>Raamat on sündinud tänu Liivi, Helje ja Urmase kihlatu Annely meenutustele ning paljude teiste intervjuudele ja lahkele kaasabile. Raamatu loomisel on kasutatud Urmase kirjutatud unikaalset Ruja päevikut, mis ilmus päevavalgele alles pärast legendi surma.</p>
         ",
            "
         <p>Georg Otsa (1920 – 1975) lauludega kasvas Eestis üles kogu sõjajärgne põlvkond. Meie jaoks on unustamatud tema Deemon, Escamillo, Figaro, Jago, Papageno, Rigoletto ja mitmed muud suurrollid. Ots laulis aga suureks ka üsna tühised meloodiad. Inimesi lummas tema kauni kõlaga lüüriline bariton, suur musikaalsus ja suurepärased näitlejavõimed. Tema meisterlikkusest ammutasid nii teater kui film, nii TV kui raadio, puhast meelelahutusmuusikat unustamata. Laulja ampluaa lihtsatest populaarsetest laulukestest meisterlike sooritusteni ooperilavadel oli ülilai, haarates kõik muusika žanrid, välja arvatud rokkmuusika, aga keegi ju ei ütle, et ta poleks ka selles silma paistnud. Kõigele lisaks oli Ots ka suurepärane joonistaja.</p>
         
         <p>Georg Otsa kui kunstnikku jumaldati kogu nõukogude impeeriumis ja eriti Venemaal, kus vaimustunud austajate hulk ulatus miljonitesse. Tohutud mõõtmed saavutanud jumaldamine ei rajanenud üksnes artisti kunstilisel võimekusel. Legendi sünniks oli vaja ka muid tegureid ja neid kahtlemata oli: korralik lastetuba, karismaatilisus, ajastusse mittesobiv elegants ja suurepärane välimus.</p>
         
         <p>Mida mõtles Georg Ots muusikast, kunstist üldse, ühiskonnast, perekonnast, oma positsioonist? Milline ta oli inimesena? Kuidas suutis jumaldatud täht jääda alati elegantselt vaoshoituks? Kõigile neile küsimustele on üritanud vastust leida raamatu autor, kes kasutades muuhulgas Eesti Raadio fonoteegis säilinud Georg Otsaga tehtud intervjuusid, paneb raamatus kõlama ka laulja enese hääle. Kulle Raig kirjutab elavalt privilegeeritud kunstnikust ja tema elust.</p>
         ",

    ];


}