<?php namespace Lightweber\Raamatud\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLightweberRaamatudRaamat extends Migration
{
    public function up()
    {
        Schema::table('lightweber_raamatud_raamat', function($table)
        {
            $table->string('highlights')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('lightweber_raamatud_raamat', function($table)
        {
            $table->dropColumn('highlights');
        });
    }
}
