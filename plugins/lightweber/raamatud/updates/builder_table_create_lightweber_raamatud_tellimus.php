<?php namespace Lightweber\Raamatud\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLightweberRaamatudTellimus extends Migration
{
    public function up()
    {
        Schema::create('lightweber_raamatud_tellimus', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('eesnimi')->nullable();
            $table->string('perenimi')->nullable();
            $table->string('uulits')->nullable();
            $table->string('maja')->nullable();
            $table->string('korter')->nullable();
            $table->string('indeks')->nullable();
            $table->string('asula')->nullable();
            $table->string('maakond')->nullable();
            $table->string('email')->nullable();
            $table->string('telefon')->nullable();
            $table->string('mobiil')->nullable();
            $table->string('transport')->nullable();
            $table->string('automaat')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->boolean('new')->nullable()->default(1);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lightweber_raamatud_tellimus');
    }
}
