<?php namespace Lightweber\Raamatud\Updates;

use Lightweber\Raamatud\Models\Pakk;
use Seeder;

class Seeder104 extends Seeder
{
    protected $colors = [
        '#4dc7e0',
        '#28bcad',
        '#bfd62e',
        '#fdb933',
        '#ee4c8f',
        '#7c80be',
        '#4dc7e0',
    ];
    public function run()
    {
        Pakk::create([
            'nr' => 0,
            'type' => 'free',
            'color' => $this->colors[0],
        ]);

        for ($i = 1; $i < count($this->colors);$i++)
        {
            Pakk::create([
                'nr' => $i,
                'type' => 'pack',
                'color' => $this->colors[$i],
            ]);
        }
    }
}