<?php namespace Lightweber\Raamatud\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLightweberRaamatudAutomaat extends Migration
{
    public function up()
    {
        Schema::create('lightweber_raamatud_automaat', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('kood');
            $table->string('nimetus');
            $table->string('aadress');
            $table->string('maakond');
            $table->primary(['kood']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lightweber_raamatud_automaat');
    }
}
