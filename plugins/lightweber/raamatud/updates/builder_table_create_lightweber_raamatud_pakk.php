<?php namespace Lightweber\Raamatud\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLightweberRaamatudPakk extends Migration
{
    public function up()
    {
        Schema::create('lightweber_raamatud_pakk', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('nr')->nullable();
            $table->enum('type', ['pack', 'free'])->default('pack');
            $table->string('color')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('lightweber_raamatud_pakk');
    }
}
