<?php namespace Lightweber\Raamatud\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLightweberRaamatudRaamat extends Migration
{
    public function up()
    {
        Schema::create('lightweber_raamatud_raamat', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->string('nimi')->nullable();
            $table->string('autor')->nullable();
            $table->text('sisu')->nullable();
            $table->integer('pakk_id')->nullable();
            $table->string('pilt')->nullable();
            $table->date('date')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('lightweber_raamatud_raamat');
    }
}
