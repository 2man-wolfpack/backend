<?php namespace Lightweber\Raamatud;

use LightWeber\Raamatud\Components\Myygikohad;
use LightWeber\Raamatud\Components\PakiautomaadidComponent;
use LightWeber\Raamatud\Components\PakidComponent;
use LightWeber\Raamatud\Components\RaamatComponent;
use LightWeber\Raamatud\Components\TellimiseComponent;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            PakidComponent::class           => 'pakidComponent',
            RaamatComponent::class          => 'raamatComponent',
            TellimiseComponent::class       => 'tellimiseComponent',
            PakiautomaadidComponent::class  => 'pakiautomaadidComponent',
            Myygikohad::class               => 'myygikohad',
        ];
    }

    public function registerSettings()
    {
    }
}
