<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

Route::post('telli-kingitus', function (Request $request) {

    \Lightweber\Raamatud\Models\Tellimus::create($request->all());

    return Redirect::to('/aitah');
});
