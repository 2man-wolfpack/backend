<?php namespace Lightweber\Raamatud\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Lightweber\Raamatud\Models\Tellimus;

class Exports extends Controller
{

    const file_path = 'storage/app/failid';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Lightweber.Raamatud', 'raamatud', 'exports');
    }

    public function index()
    {
        if ( ! is_dir(self::file_path))
        {
            mkdir(self::file_path);
        }

        $files = scandir(self::file_path, SCANDIR_SORT_DESCENDING);
        array_pop($files);
        array_pop($files);
        $this->vars['tellimused'] = Tellimus::where('new', '=', 1)->get();
        $this->vars['files'] = $files;
    }

    public function onExport()
    {
        $tellimused = Tellimus::where('new', '=', 1);


        $text = $this->renderExportFile($tellimused->get());


        $filename = time() . '.txt';

        file_put_contents(self::file_path . '/' . $filename, $text);

        if (input('real'))
        {
            $tellimused->update(['new' => 0]);
        }

        return Redirect::refresh()->with('newfile', $filename);
    }


    private function renderExportFile($tellimused)
    {
        $text = '';

        foreach($this->mapping as $key => $value)
        {
            $text .= $key . "\t";
        }

        $text = trim($text) . "\n";

        foreach ($tellimused as $t)
        {
            foreach($this->mapping as $key => $array)
            {
                if (isset($array['value']))
                {
                    $text .= $array['value'] . "\t";
                }
                else if(isset($array['field']))
                {
                    $text .= object_get($t, $array['field']) . "\t";
                }
                else
                {
                    $text .= '{' . $key . "}\t";
                }
            }
            $text = trim($text) . "\n";

            $text = $this->replaceValues($t, $text);
        }
        return $text;
    }

    public function getfile()
    {
        return response()->download(self::file_path. '/' . input('file'));
    }


    private function replaceValues(Tellimus $t, $text)
    {
        if ($t->transport === 'automaat')
        {
            $rkmkood = 'POST24';
            $kattetoimviis = 5;
            $kattetoimnimi = 'Post24';
            $paki_kood = $t->automaat;
            $paki_aadress = $t->pakiautomaat->aadress;
        }
        else {
            $rkmkood = 'KULLER';
            $kattetoimviis = 1;
            $kattetoimnimi = '';
            $paki_kood = '';
            $paki_aadress = '';

        }

        $aadress = $t->asula . ', ' .$this->maakonnad[$t->maakond];
        $text = str_replace('{maadress}', $aadress, $text);
        $text = str_replace('{saadress}', $aadress, $text);

        $tellkpv = Carbon::parse($t->created_at)->format('d.m.Y');
        $text = str_replace('{tellkpv}', $tellkpv, $text);

        $text = str_replace('{rkmkood}', $rkmkood, $text);
        $text = str_replace('{KATTETOIM_VIIS}', $kattetoimviis, $text);
        $text = str_replace('{KATTETOIM_NIMI}', $kattetoimnimi, $text);
        $text = str_replace('{PAKI_KOOD}', $paki_kood, $text);
        $text = str_replace('{PAKI_AADR}', $paki_aadress, $text);

        return $text;
    }

    protected $maakonnad = [
      'harjumaa' => 'Harjumaa',
      'hiiumaa' => 'Hiiumaa',
      'ida-virumaa' => 'Ida-Virumaa',
      'jogevamaa' => 'Jõgevamaa',
      'jarvamaa' => 'Järvamaa',
      'laanemaa' => 'Läänemaa',
      'laane-virumaa' => 'Lääne-Virumaa',
      'polvamaa' => 'Põlvamaa',
      'parnumaa' => 'Pärnumaa',
      'raplamaa' => 'Raplamaa',
      'saaremaa' => 'Saaremaa',
      'tartumaa' => 'Tartumaa',
      'valgamaa' => 'Valgamaa',
      'viljandimaa' => 'Viljandimaa',
      'vorumaa' => 'Võrumaa',
    ];


    protected $mapping = [
        'algus' => ['value' => '5/4/17',],
        'lopp' => ['value' => '5/4/17',],
        'arvenr' => ['value' => '',],
        'eksempla' => ['value' => 1,],
        'kanal' => ['value' => 'WEB',],
        'maadress' => [],
        'maksumus' => ['value' => '',],
        'memail' => ['field' => 'email'],
        'menimi' => ['field' => 'nimi'],
        'mfax' => ['value' => '',],
        'mindeks' => ['field' => 'indeks'],
        'misikukood' => ['value' => '',],
        'mkorter' => ['field' => 'korter'],
        'mmaja' => ['field' => 'maja'],
        'mmobiil' => ['field' => 'mobiil'],
        'mnimi' => ['field' => 'nimi'],
        'mtanav' => ['field' => 'uulits'],
        'mtelefon' => ['field' => 'telefon'],
        'pank' => ['value' => '',],
        'rkmkood' => [],
        'saadress' => [],
        'semail' => ['field' => 'email'],
        'senimi' => ['field' => 'nimi'],
        'sfax' => ['value' => '',],
        'sindeks' => ['field' => 'index'],
        'sisikukood' => ['value' => '',],
        'skorter' => ['field' => 'korter'],
        'smaja' => ['field' => 'maja'],
        'smobiil' => ['field' => 'mobiil'],
        'snimi' => ['field' => 'nimi'],
        'stanav' => ['field' => 'uulits'],
        'stelefon' => ['field' => 'telefon'],
        'tellkpv' => [],
        'tlkood' => ['value' => '',],
        'trykiarve' => ['value' => '0',],
        'trykiokpakkumine' => ['value' => '0',],
        'tyyp' => ['value' => 'U',],
        'vaindeks' => ['value' => '2400',],
        'viitenumber' => ['value' => '',],
        'vvotja' => ['value' => 'EP',],
        'kinnitatud' => ['value' => '1',],
        'makstud' => ['value' => '0',],
        'rhkkood' => ['value' => '423334895',],
        'KATTETOIM_VIIS' => [],
        'KATTETOIM_NIMI' => [],
        'PAKI_KOOD' => [],
        'PAKI_AADR' => [],
    ];
}