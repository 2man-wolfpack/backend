<?php namespace LightWeber\Raamatud\Components;

use Cms\Classes\ComponentBase;
use Lightweber\Raamatud\Models\Raamat;

class RaamatComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Raamat',
            'description' => 'Raamatu detailid'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function raamat()
    {
        return Raamat::where('slug', '=', $this->param('slug'))->first();
    }

    public function nextRaamat()
    {
        $raamat = $this->raamat();

        $nextRaamat = Raamat::where('id', '=', $raamat->id + 1);

        if($nextRaamat->count())
        {
            return $nextRaamat->first()->slug;
        }
        return null;
    }

    public function prevRaamat()
    {
        $raamat = $this->raamat();

        $prevRaamat = Raamat::where('id', '=', $raamat->id - 1);

        if($prevRaamat->count())
        {
            return $prevRaamat->first()->slug;
        }
        return null;
    }
}
