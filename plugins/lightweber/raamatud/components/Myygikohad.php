<?php namespace LightWeber\Raamatud\Components;

use Cms\Classes\ComponentBase;

class Myygikohad extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Myygikohad Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function onUpdateCounty()
    {
        $this->page['myygikohad'] = $this->myygikohad[input('county')];
    }


    protected $myygikohad = [
        'harjumaa' => [
            'Apollo Viimsi, Harju m/k, Sõpruse tee 15, Viimsi vald',
            'Comarket Laulasmaa, Harju m/k, Laulasmaa, Siilivase, Keila vald',
            'Delice Viimsi, Harju m/k, Viimsi v, Randvere tee 6, Viimsi vald',
            'Grossi Kehra, Harju m/k, Kehra, Kose mnt 7, Kehra linn ',
            'Grossi Keila, Keila, Piiri 5, Keila linn ',
            'Grossi Kose, Kodu 2, Kose, Kose vald',
            'Grossi Viimsi Toidupood, Harju m/k, Viimsi, Kaluri tee 3., Viimsi vald',
            'Hyper Rimi Laagri, Harju m/k Pärnu mnt. 556, Saue vald',
            'Hyper Rimi Viimsi, Randvere tee 9, Viimsi, Viimsi vald',
            'Konsum Aruküla, Harju m/k, Aruküla, Tallinna mnt.8, Raasiku vald',
            'Konsum Haabneeme, Harju m/k, Haabneeme, Rohuneeme mnt.32, Viimsi vald',
            'Konsum Jüri, Harju m/k, Jüri, Aruküla tee 29, Rae vald',
            'Konsum Kehra, Harju m/k, Kehra,Kose mnt.9, Kehra linn ',
            'Konsum Kiili, Harju m/k, Kiili, Nabala tee 4, Kiili vald',
            'Konsum Kose, Harju m/k, Kose, Pikk 23, Kose vald',
            'Konsum Kuusalu, Harju m/k, Kuusalu, Kuusalu 13, Kuusalu vald',
            'Konsum Loksa, Harju m/k, Loksa, Tallinna 36, Loksa linn ',
            'Konsum Miiduranna, Ranna tee 46a, Viimsi vald',
            'Konsum Muraste, Harju m/k, Muraste küla, Lee tee 1, Harku vald',
            'Konsum Paldiski, Harju m/k, Paldiski, Rae 38, Paldiski linn',
            'Konsum Raasiku, Harju m/k, Raasiku, Tallinna mnt.19, Raasiku vald',
            'Konsum Saku, Harju m/k, Saku, Pargi 2, Saku vald',
            'Kostivere kauplus, Harju m/k, Kostivere, Liukivi 2, Jõelähtme vald',
            'Loksa pood, Harju m/k, Loksa, Posti 2a, Loksa linn ',
            'Loo pood, Kaare tee 2, Loo alevik, Jõelähtme vald',
            'Maksimarket Laagri, Harju m/k, Laagri, Pärnu mnt. 558a, Saue vald',
            'Nurga kauplus Kuusalu , Harju m/k, Kuusalu v, Kuusalu tee 39, Kuusalu vald',
            'Riisipere pood, Harju m/k, Riisipere, Metsa 2, Nissi vald',
            'Rõõmu Kaubamaja, Harju m/k, Keila, Haapsalu mnt.57b, Keila linn ',
            'Saue Kaubakeskus, Harju m/k, Saue, Ridva 15, Saue linn',
            'Selver Keila, Harju m/k, Keila, Piiri tn.12, Keila linn ',
            'Selver Peetri, Veesaare tee 2, Peetri alevik, Rae vald, Rae vald',
            'Selver Saku, Harju m/k, Saku, Üksnurme tee 2, Saku vald',
            'Selver Viimsi, Sõpruse tee 15, Viimsi vald',
        ],
        'hiiumaa' => [
            'Konsum Käina, Hiiumaa, Käina v, Käina alev, Hiiu mnt. 9, Käina vald',
            'Konsum Kärdla, Hiiumaa, Kärdla, Keskväljak 1, Kärdla linn',
            'Konsum Tormi, Hiiumaa, Kärdla, Heltemaa mnt. 14a, Kärdla linn',
            'Selver Hiiumaa, Hiiumaa, Linnumäe küla, Rehemäe, Pühalepa vald',
        ],
        'ida-virumaa' => [
            'Aldar Purtse kauplus, I.-Virumaa, Lüganuse v,  Purtse küla, Lüganuse vald',
            'Avinurme pood , I.-Virumaa, Avinurme, Rakvere tn. 1, Avinurme vald',
            'Grossi Ahtme Toidukaubad, I.-Virumaa, Ahtme, Estonia pst. 30a, Kohtla-Järve linn',
            'Grossi Iisaku Toidukaubad, I.-Virumaa, Iisaku, Tartu mnt.55, Iisaku vald',
            'Grossi Jõhvi Toidukaubad, I.-Virumaa, Jõhvi, Tartu mnt.15a, Jõhvi linn',
            'Iisaku Ostukeskus, I.-Virumaa, Iisaku v, Tartu mnt.46, Iisaku vald',
            'Konsum Kiviõli, I.-Virumaa, Kiviõli, Keskpuiestee 31, Kiviõli linn  ',
            'R-kiosk  Jõhvi Bussijaam, I.-Virumaa, Jõhvi, Narva mnt.4 a, Jõhvi linn',
            'Selver Jõhvi, I.-Virumaa, Jõhvi Narva mnt.4a, Jõhvi linn',
            'Selver Kohtla-Järve, I.-Virumaa, K.-Järve, Järveküla tee 68, Kohtla-Järve linn',
            'Toila kauplus, I.-Virumaa, Toila, Pikk 18a, Toila vald',
        ],
        'jarvamaa' => [
            'Aravete pood, Järva m/k, Aravete, Piibe 16, Ambla vald',
            'Grossi Järva-Jaani Toidukauplus, Järva m/k, Järva-Jaani, Lai tn 23, Järva-Jaani vald',
            'Grossi Koeru Toidukaubad, Järva m/k, Koeru, Paide tee 2, Koeru vald',
            'Grossi Paide Toidukaubad, Järva m/k, Paide, Pikk tn.25, Paide linn',
            'Grossi Türi toidukaubad, Järva m/k, Türi, Viljandi mnt.13a, Türi linn',
            'Konsum Järva-Jaani, Järva m/k, J-Jaani, Jaani 1, Järva-Jaani vald',
            'Konsum Kalevi Türi, Järva m/k, Türi, Kalevi 1, Türi linn',
            'Konsum Koeru, Järva m/k, Koeru, Paide tee 2, Koeru vald',
            'Konsum Paide, Järva m/k, Paide, Keskväljak 15, Paide linn',
            'Konsum Türi, Järva m/k, Türi, Tallinna 4, Türi linn',
            'Maksimarket Paide, Järva m/k, Paide, Ringtee 2, Paide linn',
            'Selver Paide, Järva m/k, Paide, Aiavilja 4, Paide linn',
            ],
        'jogevamaa' => [
            'Grossi Jõgeva Toidukaubad, Jõgeva linn, Tähe 10a, Jõgeva linn',
            'Keskuse kauplus, Jõgeva linn, Aia 1, Jõgeva linn',
            'Konsum Jõgeva Kaubahall, Jõgeva linn, Aia 3, Jõgeva linn',
            'Konsum Mustvee, Jõgeva m/k, Mustvee, Tartu 3, Mustvee linn',
            'Konsum Pae, Jõgeva linn, Aia tn 33, Jõgeva linn',
            'Mustvee pood, Jõgeva m/k, Mustvee, Tartu 7, Mustvee linn',
            'Rüütli kauplus, Jõgeva m/k, Põltsamaa, Lossi 11, Põltsamaa linn',
            'Selver Jõgeva, Jõgeva linn, Kesk 3a/4, Jõgeva linn',
            'Selver Põltsamaa, Jõgeva m/k, Põltsamaa, Jõgeva mnt 1a, Põltsamaa linn',
            ],
        'laane-virumaa' => [
            'Aldar Lilleoru kauplus, L.-Virumaa, Rakvere, Tartu tn. 55, Rakvere linn',
            'Aldar Pajusti kauplus, L.-Virumaa, Pajusti, Tartu mnt. 13, Vinni vald',
            'Aldar Simuna kauplus, L.-Virumaa, Simuna, Allika 1, Väike-Maarja vald',
            'Apollo Rakvere Kroonikeskus, L.-Virumaa, Rakvere, F.G. Adoffi 11, Rakvere linn',
            'Grossi GEA kauplus, L.-Virumaa, Haljala, Võsu mnt.5, Haljala vald',
            'Grossi Kunda Toidukaubad, L.-Virumaa, Kunda Kasemäe 12, Kunda linn',
            'Grossi Raja kauplus, L.-Virumaa, Rakvere, Ilu pst.2, Rakvere linn',
            'Grossi Rakke pood, L.-Virumaa, Rakke, Faehlmanni 38, Rakke vald',
            'Grossi Tapa Kaubanduskeskus, L.-Virumaa, Tapa, Jaama 1, Tapa linn',
            'Grossi Turukaubamaja, L.-Virumaa, Rakvere, Laada 16, Rakvere linn',
            'Grossi Väike-Maarja Toidukaubad , L.-Virumaa, Väike-Maarja, Pikk tn.9, Väike-Maarja vald',
            'Hyper Rimi Rakvere, L.-Virumaa, Tõrremäe küla, Rakvere linn',
            'Kaseke kauplus, L.-Virumaa, Tapa, Ülesõidu 8, Tapa linn',
            'Keskuse pood, L.-Virumaa, Võsu, Mere 67, Vihula vald',
            'Konsum Kadrina, L.-Virumaa, Kadrina, Viitna tee 4, Kadrina vald',
            'Maksimarket Rakvere, L.-Virumaa, Rakvere, Lõõtspilli 2, Rakvere linn',
            'Nord Kaubahall, L.-Virumaa, Rakvere, Tallinna tn.68, Rakvere linn',
            'Selver Krooni, L.-Virumaa, Rakvere, F. G. Adoffi 11, Rakvere linn',
            'Tamsalu Kaubamaja, L.-Virumaa, Tamsalu, Raudtee 5, Tamsalu linn',
            'Tapa Kaubamaja, L.-Virumaa, Tapa, 1.mai tn.3, Tapa linn',
            'Õ-pood, L.-Virumaa, Viru-Nigula, Kunda tee 4, Viru-Nigula vald',
            ],
        'laanemaa' => [
            'Apollo Haapsalu, Lääne m/k, Haapsalu, Tallinna mnt 1, Haapsalu linn',
            'Kastani ostukeskus, Lääne m/k, Haapsalu, Kuuse 28, Haapsalu linn',
            'Konsum Haapsalu, Lääne m/k, Haapsalu, Tallinna mnt.1, Haapsalu linn',
            'Konsum Lihula, Lääne m/k, Lihula, Tallinna mnt.12, Lihula linn',
            'Konsum Uuemõisa, Lääne m/k, Uuemõisa, Tallinna mnt. 84, Haapsalu linn',
            'Konsum Virtsu, Lääne m/k, Virtsu, Tallinna mnt.13, Hanila vald',
            'Rimi Haapsalu, Lääne m/k, Haapsalu, Jaama 32, Haapsalu linn',
            'Selver Rannarootsi, Lääne m/k, Uuemõisa, Rannarootsi 1, Ridala vald',
            ],
        'parnumaa' => [
            'Apollo Kaubamajakas, Pärnu linn, Aida 7, Pärnu linn',
            'Apollo Pärnu Keskus, Pärnu linn, Lai 5, Pärnu linn',
            'Comarket Oja, Pärnu linn, Liblika 6, Pärnu linn',
            'Comarket Vana-Pärnu, Pärnu linn, Haapsalu mnt. 41, Pärnu linn',
            'Delice Pärnu, Pärnu, Lai 5, Pärnu linn',
            'Grossi Vändra Toidukaubad, Pärnu m/k, Vända, Pärnu-Paide mnt.26, Vändra vald',
            'Hyper Rimi Pärnu, Pärnu linn, Papiniidu 8, Pärnu linn',
            'Konsum Häädemeeste, Pärnu m/k, Häädemeeste v, Pärnu mnt 40, Häädemeeste vald',
            'Konsum Kilingi-Nõmme, Pärnu m/k, Kilingi-Nõmme, Pärnu 37, Kilingi-Nõmme linn',
            'Konsum Sindi, Pärnu m/k, Sindi, Jaama 8, Sindi linn',
            'Konsum Tiina, Pärnu linn, Riia mnt.74, Pärnu linn',
            'Konsum Vändra, Pärnu m/k, Vändra, Pärnu-Paide mnt. 21, Vändra vald',
            'Maksimarket Pärnu, Pärnu m/k, Papsaare, Haapsalu mnt. 43, Audru vald',
            'Port Arturi Toidukaubad, Pärnu linn, Lai 11, Pärnu linn',
            'R-kiosk  Kaubamajakas, Pärnu linn, Papiniidu 8/10, Pärnu linn',
            'R-kiosk Pärnu Haigla, Pärnu linn, Ristiku 1, Pärnu linn',
            'R-kiosk Pärnu Rüütli Combo, Pärnu linn, Rüütli 41, Pärnu linn',
            'Rae kauplus, Pärnu linn, Riia mnt. 271a, Pärnu linn',
            'Selver Mai, Pärnu linn, Papiniidu 42, Pärnu linn',
            'Selver Suurejõe, Pärnu linn, Suur-Jõe 57, Pärnu linn',
            'Selver Ülejõe, Pärnu linn, Tallinna mnt. 93a /Roheline 80, Pärnu linn',
            ],
        'polvamaa' => [
            'Eeden kauplus, Põlva m/k, Kanepi, Weizenbergi 21, Kanepi vald',
            'Konsum Edukeskus, Põlva linn, Kesk 39, Põlva linn',
            'Konsum Põlva Selvehall, Põlva linn, Jaama 16a, Põlva linn',
            'Konsum Räpina, Põlva m/k, Räpina, Kooli 14a, Räpina linn',
            'Konsum Saverna, Põlva m/k, Saverna, Põlva vald',
            'Ostumarket, Põlva linn, Vabriku 11, Põlva linn',
            'Põlva Toiduait, Põlva linn, Kesk 6a, Põlva linn',
            'Selver Põlva, Põlva linn, Jaama 12, Põlva linn',
            ],
        'raplamaa' => [
            'Edu Märjamaa, Rapla m/k, Märjamaa, Oru 1, Märjamaa vald',
            'Grossi Kohila, Rapla m/k, Kohila, Viljandi mnt.3a, Kohila vald',
            'Järvakandi pood , Pargi 1, Järvakandi alev, Järvakandi vald',
            'Karmani kauplus  , Rapla linn, Kooli 6a, Rapla linn',
            'Konsum Jaama, Rapla linn, Viljandi mnt. 126, Rapla linn',
            'Konsum Kandi, Rapla m/k, Järvakandi, Tallinna mnt. 37, Järvakandi vald',
            'Konsum Kohila, Rapla m/k, Kohila, Lõuna 2, Kohila vald',
            'Konsum Rapla, Rapla linn, Tallinna mnt. 16, Rapla linn',
            'Märjamaa pood, Pärnu mnt 31A, Märjamaa alev, Märjamaa vald',
            'Selver Rapla, Tallinna mnt. 4, Rapla, Rapla linn',
            ],
        'saaremaa' => [
            'Konsum Liiva, Saaremaa, Muhu v, Liiva k. , Muhu vald',
            'Konsum Orissaare , Saaremaa, Orissaare, Kuivastu mnt. 28, Orissaare vald',
            'Konsum Rae , Saaremaa, Kuressaare, Raekoja 10, Kuressaare linn ',
            'Konsum Tooma, Saaremaa, Kuressaare, Smuuli 1, Kuressaare linn ',
            'Rimi Kuressaare, Saaremaa, Kaarma v, Tallinna mnt.88, Kuressaare linn ',
            'Saarlase Puhvet, Saaremaa, Kuressaare, Pihtla tee 2, Kuressaare linn ',
            'Selver Saare, Saaremaa, Kuressaare, Tallinna mnt. 67, Kuressaare linn ',
            ],
        'tallinn' => [

            'Apollo Järve, Tallinn, Pärnu mnt. 238, Tallinn Nõmme',
            'Apollo Kristiine, Tallinn, Endla 45, Tallinn Kristiine',
            'Apollo Lasnamäe, Mustakivi tee 13, Tallinn Lasnamäe',
            'Apollo Mustamäe, Tallinn, Tammsaare tee 104a, Tallinn Mustamäe',
            'Apollo Solaris, Tallinn, Estonia pst. 9, Tallinn Kesklinn',
            'Apollo Ülemiste, Tallinn, Suur-Sõjamäe 4, Tallinn Lasnamäe',
            'Comarket Marja, Tallinn, Mustamäe tee 45, Tallinn Mustamäe',
            'Comarket Nõmmekeskus, Tallinn, Jaama tn. 2, Tallinn Nõmme',
            'Hyper Rimi Haabersti, Tallinn, Haabersti 1, Tallinn Õismäe',
            'Hyper Rimi Mustakivi, Tallinn, Mustakivi tee 13, Tallinn Lasnamäe',
            'Hyper Rimi Norde, Tallinn, Ahtri tn. 9, Tallinn Kesklinn',
            'Hyper Rimi Sõpruse, Tallinn, Sõpruse pst. 174, Tallinn Kristiine',
            'Hyper Rimi Ülemiste, Tallinn, Suur-Sõjamäe 4, Tallinn Lasnamäe',
            'Konsum Akadeemia, Tallinn, Akadeemia tee 35, Tallinn Mustamäe',
            'Konsum Raudalu, Harju m/k, Raudalu, Viljandi mnt.41A, Tallinn Nõmme',
            'Maksimarket Peterburi tee, Tallinn, J.Smuuli tee 43, Tallinn Lasnamäe',
            'Prisma Kristiine, Tallinn, Endla 45, Tallinn Kristiine',
            'Prisma Lasnamäe, Tallinn, Mustakivi tee 17, Tallinn Lasnamäe',
            'Prisma Mustamäe, Tallinn, Tammsaare tee 116, Tallinn Mustamäe',
            'Prisma Rocca al Mare, Tallinn, Paldiski mnt. 102, Tallinn Õismäe',
            'Prisma Sikupilli, Tallinn, Tartu mnt. 87, Tallinn Kesklinn',
            'Pääsküla kauplus, Tallinn, Pärnu mnt. 421, Tallinn Nõmme',
            'R-kiosk Kristiine, Tallinn, Endla 45, Tallinn Kristiine',
            'R-kiosk Kristiine 2, Tallinn, Kristiine keskus, Endla 45, Tallinn Kristiine',
            'R-kiosk Magistrali, Tallinn, Sõpruse pst. 201/203, Tallinn Mustamäe',
            'R-kiosk Cafe Pärnu , Tallinn, Pärnu mnt.139a, Tallinn Kesklinn',
            'R-kiosk Neste Peterburi, Tallinn, Peterburi tee 48, Tallinn Lasnamäe',
            'R-kiosk Neste Punane, Tallinn, Punane 43, Tallinn Lasnamäe',
            'R-kiosk Neste Kadaka, Tallinn, Kadaka tee 60, Tallinn Mustamäe',
            'R-kiosk Neste Forelli, Tallinn, Mustamäe tee 22/Forelli 2, Tallinn Mustamäe',
            'R-kiosk Neste Laagri, Tallinn, Pärnu mnt. 453, Tallinn Nõmme',
            'R-kiosk Neste Rocca-al-Mare, Tallinn, Paldiski mnt. 98, Tallinn Õismäe',
            'R-kiosk Neste Sõle, Tallinn, Sõle 25a, Tallinn Kopli',
            'R-kiosk Tallinn bussijaam, Tallinn, bussijaam, Tallinn Kesklinn',
            'R-kiosk Roosikrantsi, Tallinn, Roosikrantsi tn.2, Tallinn Kesklinn',
            'R-kiosk Rautakesko Tondi, Tallinn, Tammsaare tee 49, Tallinn Mustamäe',
            'R-kiosk Viru Keskus, Tallinn, Viru Keskus, Tallinn Kesklinn',
            'R-kiosk Viru bussiterminal, Tallinn, Viru bussiterminaal, Tallinn Kesklinn',
            'R-kiosk Lennujaam, Tallinn, Lennujaama tee 2, Tallinn Kesklinn',
            'R-kiosk Lennujaam , Tallinn, Lennujaama tee 2, Tallinn Lasnamäe',
            'R-kiosk Laikmaa 2, Tallinn, Laikmaa 2, Tallinn Kesklinn',
            'R-kiosk Vabaduse väljak 7 tunnelis, Tallinn, Vabaduse väljak 7, Tallinn Kesklinn',
            'Rimi Magistrali, Tallinn, Sõpruse pst. 201/203, Tallinn Mustamäe',
            'Rimi Pirita, Pirita, Merivälja tee 28, Tallinn Pirita',
            'Rimi Põhja, Tallinn, Põhja pst.17, Tallinn Kopli',
            'Rimi Tammsaare, Tammsaare tee 104a, Tallinn Mustamäe',
            'Selver Arsenali, Tallinn, Erika 17, Tallinn Kopli',
            'Selver Järve, Tallinn, Pärnu mnt. 238, Tallinn Nõmme',
            'Selver Kadaka, Tallinn, Kadaka tee 56a, Tallinn Mustamäe',
            'Selver Kakumäe, Tallinn, Rannamõisa tee 6, Tallinn Õismäe',
            'Selver Kärberi, Tallinn, Kärberi 20/20a, Tallinn Lasnamäe',
            'Selver Läänemere, Tallinn, Läänemere tee 28, Tallinn Lasnamäe',
            'Selver Marienthali, Tallinn, Mustamäe tee 16, Tallinn Kristiine',
            'Selver Merimetsa, Tallinn, Paldiski mnt.  56, Tallinn Õismäe',
            'Selver Mustakivi, Tallinn, Mustakivi tee  3a, Tallinn Lasnamäe',
            'Selver Pelgulinna, Tallinn, Sõle 51, Tallinn Kopli',
            'Selver Pirita, Tallinn, Rummu tee 2, Tallinn Pirita',
            'Selver Punane, Tallinn, Punane 46, Tallinn Lasnamäe',
            'Selver Tondi, Tallinn, Tammsaare tee 62, Tallinn Mustamäe',
            'Selver Torupilli, Tallinn, Vesivärava 37, Tallinn Lasnamäe',
            'Solaris Toidukaubad, Tallinn, Estonia pst. 9, Tallinn Kesklinn',
            'Stockmann, Tallinn, Liivalaia 53, Tallinn Kesklinn',
            ],
        'tartumaa' => [
            'A-Market kauplus, Tartu m/k, Elva, Valga mnt. 7, Elva linn',
            'Apollo Lõunakeskus, Tartu linn, Ringtee 75, Tartu linn',
            'Apollo Tartu Kaubamaja, Tartu linn, Riia mnt. 1, Tartu linn',
            'Grossi Tartu Toidukaubad, Tartu linn, Soola 7, Tartu linn',
            'Haraldi Nõo , Tartu m/k, Nõo, Oja 12, Nõo vald',
            'Hyper Rimi Lõunakeskus, Tartu linn, Ringtee 75, Tartu linn',
            'Hyper Rimi Tartu, Tartu linn, Rebase 10, Tartu linn',
            'Karlova kauplus, Tartu linn, Võru mnt.47a, Tartu linn',
            'Konsum Arbimäe, Tartu m/k, Elva, Kirde 3, Elva linn',
            'Konsum Elva, Tartu m/k, Elva, Kesk 1, Elva linn',
            'Konsum Kambja, Tartu m/k, Kambja, Kesk 2A, Kambja vald',
            'Konsum Puhja, Tartu m/k, Puhja, Nooruse 2, Puhja vald',
            'Konsum Rõngu, Rõngu, Tartu mnt.2, Rõngu vald',
            'Konsum Ülenurme, Tartu m/k, Ülenurme, Võru mnt.2, Ülenurme vald',
            'Prisma Tartu Annelinna, Tartu linn, Nõlvaku 2, Tartu linn',
            'Prisma Tartu Jõe (Sõbra), Tartu linn, Sõbra 58, Tartu linn',
            'R-kiosk Tartu Bussijaam, Tartu linn, Soola 4/Turu 2, Tartu linn',
            'R-kiosk Tartu BiteStop, Tartu linn, Ringtee 60 A Neste bensiinijaam, Tartu linn',
            'Selver Aardla, Võru 77, Tartu, Tartu linn',
            'Selver Anne, Tartu linn, Kalda tee 43, Tartu linn',
            'Selver Jaamamõisa, Tartu linn, Jaama  74, Tartu linn',
            'Selver Ringtee, Tartu linn, Aardla 114, Tartu linn',
            'Selver Sõbra, Tartu linn, Sõbra 41, Tartu linn',
            'Selver Vahi, Tartu linn, Vahi 62, Tartu linn',
            'Selver Veeriku, Tartu linn, Vitamiini 1, Tartu linn',
            ],
        'valgamaa' => [
            'Kevade keskus, Valga m/k, Tõrva, Valga tn.3, Tõrva linn',
            'Konsum Otepää, Valga m/k, Otepää, Lipuväljak 28, Otepää linn',
            'Konsum Tõrva, Valga m/k, Tõrva, Tartu tn.6, Tõrva linn',
            'Konsum Valga, Valga linn, Kuperjanovi  3, Valga linn',
            'Selver Valga, Valga linn, Raja tn.5, Valga linn',
            ],
        'viljandimaa' => [
            'Apollo Viljandi, Tallinna tn 41, Viljandi, Viljandi linn',
            'Kaare pood, Viljandi linn, Suur-Kaare 68, Viljandi linn',
            'Konsum Abja, Viljandi m/k, Abja-Paluoja, Pärnu mnt. 13, Abja-Paluoja linn',
            'Konsum Karksi-Nuia, Viljandi m/k, Karksi-Nuia, Rahumäe 1, Karksi-Nuia linn',
            'Konsum Mustla , Viljandi m/k, Mustla, Posti 52a, Tarvastu vald',
            'Konsum Suure-Jaani, Viljandi m/k, Suure-Jaani, Pärnu mnt. 3, Suure-Jaani linn',
            'Konsum Turu, Viljandi linn, Turu 16, Viljandi linn',
            'Konsum Võhma, Viljandi m/k, Võhma, Tallinna 26, Võhma linn',
            'Leola toidupood, Viljandi linn, Jakobsoni 11, Viljandi linn',
            'Maksimarket Viljandi, Viljandi linn, Lääne 2, Viljandi linn',
            'R-kiosk Viljandi bussijaam, Viljandi linn, Ilmarise 1, bussijaam, Viljandi linn',
            'Rimi Viljandi MHM, Viljandi linn, Tallinna mnt. 41, Viljandi linn',
            'Selver Centrumi, Viljandi linn, Tallinna tn 22, Viljandi linn',
            'Selver Männimäe, Viljandi linn, Riia mnt. 35, Viljandi linn',
            'Suure-Jaani pood , Viljandi m/k, Suure-Jaani, Lembitu pst. 48, Suure-Jaani vald',
            'Võhma pood , Võhma, Tallinna 28a, Võhma linn',
            ],
        'vorumaa' => [
            'Grossi Võru Toidukaubad, Võru linn, Kooli 6, Võru linn',
            'Konsum Hauka, Võru m/k, Antsla, Põllu 15, Antsla vald',
            'Konsum Vastseliina, Võru m/k, Vastseliina, Võidu 21, Vastseliina vald',
            'Konsum Võru Kesklinna, Võru linn, Vabaduse 8, Võru linn',
            'Maksimarket Võru, Võru linn, Jüri tn. 83, Võru linn',
            'Nooruse Kaubakeskus, Võru linn, Räpina mnt.1, Võru linn',
            'Rimi Võru, Võru linn, Jüri tn. 85, Võru linn',
            'Selver Vilja, Võru linn, Vilja 6, Võru linn',
            ],
    ];


}