<?php namespace LightWeber\Raamatud\Components;

use Cms\Classes\ComponentBase;
use Lightweber\Raamatud\Models\Pakiautomaat;

class PakiautomaadidComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'PakiautomaadidComponent Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function automaadid()
    {
        return Pakiautomaat::orderBy('maakond', 'asc')->orderBy('nimetus', 'asc')->get();
    }

    public function onUpdateCounty()
    {
        $this->page['pakiautomaadid'] = PakiAutomaat::where('maakond', '=', input('county'))->get();
    }
}
