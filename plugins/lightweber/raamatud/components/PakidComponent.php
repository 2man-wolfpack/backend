<?php namespace LightWeber\Raamatud\Components;

use Cms\Classes\ComponentBase;
use Lightweber\Raamatud\Models\Pakk;

class PakidComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Pakid',
            'description' => 'Pakkide nimekiri'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function pakid()
    {
        return Pakk::all();
    }
}
